<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeriodoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('periodos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('periodo', 100);
            $table->integer('valor_esperado');
            $table->integer('valor_ejecutado');
            $table->integer('porc_avance');
            $table->string('rango_calificacion', 20);
            $table->string('obs_evaluacion', 50);
            $table->string('bpin');
            $table->string('priorizada', 20);
            $table->string('observaciones', 100);
            $table->integer('prog_cof_dpto');
            $table->integer('prog_cof_nacion');
            $table->integer('prog_credito');
            $table->integer('prog_otros');
            $table->integer('prog_recursos_propios');
            $table->integer('prog_sgp_alim_escolar');
            $table->integer('prog_sgp_apsb');
            $table->integer('prog_sgp_cultura');
            $table->integer('prog_sgp_deporte');
            $table->integer('prog_sgp_educacion');
            $table->integer('prog_sgp_libre_dest');
            $table->integer('prog_sgp_libre_inv');
            $table->integer('prog_sgp_riomagdalena');
            $table->integer('prog_sgp_salud');
            $table->integer('prog_regalias');
            $table->integer('prog_total');
            $table->integer('ejec_cof_dpto');
            $table->integer('ejec_cof_nacion');
            $table->integer('ejec_credito');
            $table->integer('ejec_otros');
            $table->integer('ejec_recursos_propios');
            $table->integer('ejec_sgp_alim_escolar');
            $table->integer('ejec_sgp_apsb');
            $table->integer('ejec_sgp_cultura');
            $table->integer('ejec_sgp_deporte');
            $table->integer('ejec_sgp_educacion');
            $table->integer('ejec_sgp_libre_dest');
            $table->integer('ejec_sgp_libre_inv');
            $table->integer('ejec_sgp_riomagdalena');
            $table->integer('ejec_sgp_primera_infancia');
            $table->integer('ejec_sgp_salud');
            $table->integer('ejec_regalias');
            $table->integer('ejec_funcionamiento');
            $table->integer('ejec_gestionados');
            $table->integer('ejec_total');

            $table->integer('proyecto_id')->unsigned();
            $table->foreign('proyecto_id')->references('id')->on('proyectos');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
