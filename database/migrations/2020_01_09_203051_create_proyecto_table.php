<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProyectoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyectos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->integer('programa_id');

            $table->integer('ods_id')->unsigned()->nullable();
            $table->foreign('ods_id')->references('id')->on('ods');

            $table->integer('pilar_id')->unsigned()->nullable();
            $table->foreign('pilar_id')->references('id')->on('pilares');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    
    }
}
